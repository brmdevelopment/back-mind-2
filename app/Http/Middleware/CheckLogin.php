<?php

namespace App\Http\Middleware;

use Closure;
//use mysql_xdevapi\Exception;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $ip = $request->ip();
        if ($ip == '10.130.51.31'){
            return redirect('/');
        }
        return $next($request);
    }
}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/*Route::middleware('CheckLogin')->get('/user', function (Request $request) {

});*/

Route::group(['middleware' => ['CheckLogin']], function () {

    //Route::prefix('davivienda')->group([]);
    Route::get('checkLogin', 'CheckLoginController@checkLogin');
    Route::get('contact', 'CheckLoginController@contact');

    //controlador de productos
    Route::apiResource('products', 'ProductController');
});


